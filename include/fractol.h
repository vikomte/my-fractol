/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vic <vic@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 22:45:12 by vic               #+#    #+#             */
/*   Updated: 2016/02/26 16:36:42 by vic              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include "libft.h"
# include <mlx.h>

typedef struct		s_color
{
	int r;
	int g;
	int b;
}					t_color;

typedef struct		s_ope
{
	double			zoomx;
	double			zoomy;
	double			ox;
	double			oy;
	double			x2;
	double			y2;
	double			x;
	double			y;
	double			reel;
	double			fake;
	double			oldr;
	double			oldi;
	double			tmp;
	double			i;
	double			imax;
}					t_ope;


typedef struct		s_data
{
	void			*mlx_ptr;
	void			*win_ptr;
	void			*win_ptr2;
	void			*img_ptr;
	char			*img;
	int				bpp;
	int				endian;
	int				sl;

	int				height;
	int				width;
	t_color 		cbg;
	t_color 		mc;	
	double 			crc;

	double			imgx;
	double			imgy;
	double			cr;
	int				h;

	char 			n_arg;
	char			*arg;
	char			*arg2;

	char 			type;
	t_ope			op;
	
	char 			mmod;
	double 			mratio;
	double			mx;
	double			my;
	
	//---			rec
	char 			in;
	char 			mv;
	//---

	double			zoom;
}					t_data;

void 				show_infos(t_data *dt);
char 				valid(t_data *dt);
void				fill_bg(t_data *dt);
char 				*putn(char c, int a);
void 				construct_f(t_data *dt);
void				put_ipxl(t_data *data, t_color color, int x, int y);
t_color 			get_color(t_data *dt);
t_color				ncolor(char a, char b, char c);
void				error(char c);
int  				ssn(char *s, char *f);
void				julia(t_data *dt);
void				juliaR(t_data *dt);
void				julia_env(int x, int y, t_data *dt);
void				mandelbrot(t_data *dt);
void				burningship(t_data *dt);
void				img_init(t_data *dt);
void				zoom(t_data *dt);
void				unzoom(t_data *dt);
void				core(t_data *dt, void *win_ptr);
int 				mouse_h(int key, int mx, int my, t_data *dt);
int					key_h(int key, t_data *dt);
int					expose_h(t_data *dt);
int					refresh(t_data *dt);
int					mouse_pos(int x, int y, t_data *dt);

#endif
