# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vic <vic@student.42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/24 20:19:17 by vic               #+#    #+#              #
#    Updated: 2016/02/25 19:29:00 by vic              ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC	:= gcc

RM	:= rm -rf

MLXFLAGS := -L/usr/local/lib/ -I/usr/local/include -lmlx -framework OpenGL -framework AppKit

INC = -I include -L libft -lft

SRCS	:= $(shell find src -type f -name "*.c")

OBJS	:= $(SRCS:.c=.o)

NAME	:= fractol

all: $(NAME)

$(NAME):
	@echo " "
	@make -C libft && clear && echo "------------------------"
	@echo "\033[1;34mLIBFT BUILDED"
	@$(CC) -Wall -Wextra -Werror $(INC) $(LFT) $(MLXFLAGS) -o $(NAME) $(SRCS) && echo "Compilation done."
	@echo " "
	@echo "\033[0;31mFRACTOL \033[1;33m B u i\033[1;32m l d e d\033[0;35m..."
	@echo "\033[0;33musage: ft_fractol [file ...]"
	@echo " "
	@echo "------------------------"

clean:
	@$(RM) $(OBJS)

fclean: clean
	@$(RM) $(NAME)

re: fclean all

.PHONY: all re clean fclean