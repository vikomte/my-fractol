/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vic <vic@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/04 19:12:06 by vic               #+#    #+#             */
/*   Updated: 2016/02/26 16:40:34 by vic              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

static void init_fract(t_data *dt)
{
	dt->op.x2 = (dt->type == 'J') ? 1 : 0.6;
	dt->op.x2 = (dt->type == 'M') ? 2.1 : dt->op.x2;
	dt->op.ox = 0;
	dt->op.oy = 0;
	dt->op.y2 = 1.2;
	dt->cr = (dt->type == 'J') ? 0.285 : 0;
}

static void	init(t_data *dt)
{
	dt->mratio = -99;
	dt->crc = 4;
	dt->in = 0;
	dt->mmod = 1;
	dt->width = 700;
	dt->height = 700;
	dt->mx = dt->width / 2;
	dt->my = dt->height / 2;
	dt->zoom = .222;
	dt->cbg = ncolor(64, 51, 51);
	dt->mc = ncolor(95, 124, 199);
	if (((ssn(dt->arg, "ma") || ssn(dt->arg, "M")) && (dt->type = 'M')) ||
	((ssn(dt->arg, "ju") || ssn(dt->arg, "J")) && (dt->type = 'J')) ||
	((ssn(dt->arg, "bu") || ssn(dt->arg, "B")) && (dt->type = 'B')))
		init_fract(dt);
	else
		error('u');
	dt->win_ptr = mlx_new_window(dt->mlx_ptr, dt->width, dt->height,
			dt->arg);
	dt->op.imax = 60;
}

void		core(t_data *dt, void *win_ptr)
{
	if (!(dt->img_ptr = mlx_new_image(dt->mlx_ptr, dt->width, dt->height)))
		error('m');
	if (!(dt->img = mlx_get_data_addr(dt->img_ptr, &dt->bpp, &dt->sl, &dt->endian)))
		error('m');
	mlx_hook(win_ptr, 2, (1L << 0), key_h, dt);
	mlx_hook(win_ptr, 6, (1L << 6), mouse_pos, dt);
	mlx_mouse_hook(dt->win_ptr, mouse_h, dt);
	mlx_expose_hook(dt->win_ptr, expose_h, dt);
	mlx_loop_hook(dt->mlx_ptr, refresh, dt);
	mlx_loop(dt->mlx_ptr);
}

int			main(int ac, char **av)
{
	t_data	dt;
	int 	a;

	a = 0;
	if (!av[1] || !ac)
		error('u');
	dt.mlx_ptr = mlx_init();
	dt.arg = av[1];
	init(&dt);
	core(&dt, dt.win_ptr);
	return (0);
}
