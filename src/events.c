/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vic <vic@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 21:38:16 by vic               #+#    #+#             */
/*   Updated: 2016/02/25 23:40:22 by vic              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

int		key_h(int k, t_data *dt)
{
	dt->mc = (k == 21) ? ncolor(74, 51, 51): dt->mc;
	dt->mc = (k == 23) ? ncolor(143, 123, 91): dt->mc;
	dt->mc = (k == 22) ? ncolor(72, 151, 141): dt->mc;
	dt->mc = (k == 26) ? ncolor(110, 101, 93): dt->mc;
	dt->crc -= (k == 8 && dt->crc - 2 > 0) ? 2: 0;
	dt->crc += (k == 9 && dt->crc + 2 < 20) ? 2 : 0;
	dt->mmod *= (k == 49 && dt->type == 'J') ? -1 : 1;
	dt->mratio = (k == 18 && dt->type == 'J') ? -98 : dt->mratio;
	dt->mratio = (k == 19 && dt->type == 'J') ? -99 : dt->mratio;
	dt->mratio = (k == 20 && dt->type == 'J') ? 68 : dt->mratio;
	dt->op.imax = k == 12 ? dt->op.imax + 1 : dt->op.imax + 0;
	dt->op.imax = k == 0 ? dt->op.imax - 1 : dt->op.imax + 0;
	dt->op.oy -= k == 126 ? 0.1 : 0;
	dt->op.oy += k == 125 ? 0.1 : 0;
	dt->op.ox -= k == 123 ? 0.1 : 0;
	dt->op.ox += k == 124 ? 0.1 : 0;
	dt->cbg = dt->mc;
	if (k == 53)
	{
		mlx_destroy_image(dt->mlx_ptr, dt->img_ptr);
		exit(1);
	}
	return (0);
}

int mouse_h(int key, int mx, int my, t_data *dt)
{
	dt->zoom = key == 2 ? .222 : dt->zoom;
	dt->op.ox = key == 2 ? 0 : dt->op.ox;
	dt->op.oy = key == 2 ? 0 : dt->op.oy;
	if (mx >= 0 && mx <= dt->width &&
		my >= 0 && my <= dt->height)
	{
		dt->mx = mx;
		dt->my = my;
	}
	if (mx >= 0 && mx <= dt->width && my >= 0 && my <= dt->height &&
		(key == 4 || key == 5))
		key == 5 ? unzoom(dt) : zoom(dt);
	return (0);
}

int	expose_h(t_data *dt)
{
	if (!(dt->img_ptr = mlx_new_image(dt->mlx_ptr, dt->width, dt->height)))
		error('m');
	if (!(dt->img = mlx_get_data_addr(dt->img_ptr,
	&dt->bpp, &dt->sl, &dt->endian)))
		error('m');
	construct_f(dt);
	mlx_put_image_to_window(dt->mlx_ptr, dt->win_ptr, dt->img_ptr, 0, 0);
	show_infos(dt);
	return (0);
}
