/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vic <vic@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 23:31:13 by vic               #+#    #+#             */
/*   Updated: 2016/02/25 23:57:07 by vic              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

void show_infos(t_data *dt)
{
	mlx_string_put(dt->mlx_ptr, dt->win_ptr,
	655, 10, 0x34452A, ft_itoa(dt->op.imax));
	mlx_string_put(dt->mlx_ptr, dt->win_ptr,
	586, 10, 0x34452A, "imax ");
	mlx_string_put(dt->mlx_ptr, dt->win_ptr,
	655, 30, 0x34452A, ft_itoa(dt->op.ox * 10));
	mlx_string_put(dt->mlx_ptr, dt->win_ptr,
	586, 30, 0x34452A, "ox   ");
	mlx_string_put(dt->mlx_ptr, dt->win_ptr,
	655, 50, 0x34452A, ft_itoa(dt->op.oy * 10));
	mlx_string_put(dt->mlx_ptr, dt->win_ptr,
	586, 50, 0x34452A, "oy   ");
	mlx_string_put(dt->mlx_ptr, dt->win_ptr,
	655, 70, 0x34452A, ft_itoa(dt->zoom * 10));
	mlx_string_put(dt->mlx_ptr, dt->win_ptr,
	586, 70, 0x34452A, "zoom ");
	mlx_string_put(dt->mlx_ptr, dt->win_ptr,
	655, 90, 0x34452A, ft_itoa(ABS(dt->mratio)));
	mlx_string_put(dt->mlx_ptr, dt->win_ptr,
	586, 90, 0x34452A, "ratio");
}

void	put_ipxl(t_data *data, t_color color, int x, int y)
{
	int	r;

	color.r += 0;
	r = (x * data->bpp / 8) + (y * data->sl);
	if ((x < data->width && y < data->height && x >= 0 && y >= 0))
	{
		(data->img)[r++] = !data->endian ? color.b : color.r;
		(data->img)[r++] = !data->endian ? color.g : color.b;
		(data->img)[r] = !data->endian ? color.r : color.g;
	}
}

void	zoom(t_data *dt)
{
	double pas[2];
	double dmsx;
	double dmsy;
	double ds;
	double h2;
	double w2;

	ds = 320;
	h2 = dt->height / 2;
	w2 = dt->width / 2;
	dmsx = ABS(dt->mx - w2);
	dmsy = ABS(dt->my - h2);
	dt->zoom *= 1.1;
	pas[0] = (0.0333 / dt->zoom) * (double)((int)dmsx / 100);
	pas[1] = (0.0333 / dt->zoom) * (double)((int)dmsy / 100);
	if (dt->mx > 0 && dt->mx < w2)
		dt->op.ox -= pas[0];
	if (dt->mx > w2)
		dt->op.ox += pas[0];
	if (dt->my > 0 && dt->my < h2)
		dt->op.oy -= pas[1];
	if (dt->my > h2)
		dt->op.oy += pas[1];
}

int	refresh(t_data *dt)
{
	mlx_clear_window(dt->mlx_ptr, dt->win_ptr);
	expose_h(dt);
	return (0);
}

int	mouse_pos(int x, int y, t_data *dt)
{
	if (x >= 0 && y >= 0 && x <= dt->width && y <= dt->height)
	{
		dt->mx = x;
		dt->my = y;
	}
	return (0);
}
