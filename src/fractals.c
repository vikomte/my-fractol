/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractals.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vic <vic@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 23:35:29 by vic               #+#    #+#             */
/*   Updated: 2016/02/25 23:35:36 by vic              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <fractol.h>

void		julia(t_data *dt)
{
	if (dt->mmod > 0)
		dt->mratio = (dt->mx - dt->my);
	dt->op.reel = dt->mratio / 190;
	dt->op.fake = dt->mratio / 190;
	dt->op.oldr = (dt->op.x - (dt->width / 2)) / (dt->zoom *
	dt->width) + dt->op.ox;
	dt->op.oldi = (dt->op.y - (dt->height / 2)) / (dt->zoom *
	dt->height) + dt->op.oy;
	dt->op.i = 0;
	while (dt->op.oldr * dt->op.oldr + dt->op.oldi * dt->op.oldi < 4 && dt->op.i
			< dt->op.imax)
	{
		dt->op.tmp = dt->op.oldr;
		dt->op.oldr = dt->op.oldr * dt->op.oldr - dt->op.oldi * dt->op.oldi
			+ dt->op.reel;
		dt->op.oldi = 2 * dt->op.oldi * dt->op.tmp + dt->op.fake;
		dt->op.i++;
	}
		put_ipxl(dt, get_color(dt), dt->op.x, dt->op.y);
}

void		mandelbrot(t_data *dt)
{
	dt->op.reel = (dt->op.x - (dt->width / 2)) / (dt->zoom *
	dt->width) + dt->op.ox;
	dt->op.fake = (dt->op.y - (dt->height / 2)) / (dt->zoom *
	dt->height) + dt->op.oy;
	dt->op.oldr = 0;
	dt->op.oldi = 0;
	dt->op.i = 0;
	while ((dt->op.oldr * dt->op.oldr + dt->op.oldi * dt->op.oldi) < 4 &&
			dt->op.i < dt->op.imax)
	{
		dt->op.tmp = dt->op.oldr;
		dt->op.oldr = dt->op.oldr * dt->op.oldr - dt->op.oldi * dt->op.oldi
			+ dt->op.reel;
		dt->op.oldi = 2 * dt->op.oldi * dt->op.tmp + dt->op.fake;
		dt->op.i++;
	}
		put_ipxl(dt, get_color(dt), dt->op.x, dt->op.y);
}

void		burningship(t_data *dt)
{
	dt->op.reel = ((dt->op.x - (dt->width / 2)) / (dt->zoom *
	dt->width)) + dt->op.ox;
	dt->op.fake = ((dt->op.y - (dt->height / 2)) / (dt->zoom *
	dt->height)) + dt->op.oy;
	dt->op.oldr = 0;
	dt->op.oldi = 0;
	dt->op.i = 0;
	while ((dt->op.oldr * dt->op.oldr + dt->op.oldi * dt->op.oldi) < 4 &&
			dt->op.i < dt->op.imax)
	{
		dt->op.tmp = dt->op.oldr;
		dt->op.oldr = dt->op.oldr * dt->op.oldr - dt->op.oldi * dt->op.oldi
			+ dt->op.reel;
		dt->op.oldi = 2 * ABS(dt->op.oldi * dt->op.tmp) + dt->op.fake;
		dt->op.i++;
	}
		put_ipxl(dt, get_color(dt), dt->op.x, dt->op.y);
}
