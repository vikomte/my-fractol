#include "fractol.h"

t_color		ncolor(char a, char b, char c)
{
	t_color co;

	co.r = a;
	co.g = b;
	co.b = c;
	return (co);
}

t_color icolor(t_data *dt, t_color c)
{
	if (dt->op.i > 0 && dt->op.i < (dt->op.imax / 7))
		c.r -= (c.r % 200) / dt->crc;
	if (dt->op.i > (dt->op.imax / 7) &&
	dt->op.i < ((dt->op.imax / 7) * 2))
		c.g -= (c.g % 200) / dt->crc;
	if (dt->op.i > ((dt->op.imax / 7) * 2) &&
	dt->op.i < ((dt->op.imax / 7) * 3))
		c.b -= (c.b % 200) / dt->crc;
	if (dt->op.i > ((dt->op.imax / 7) * 3) &&
	dt->op.i < ((dt->op.imax / 7) * 4))
		c.r -= (c.r % 200) / dt->crc * 2;
	if (dt->op.i > ((dt->op.imax / 7) * 4) &&
	dt->op.i < ((dt->op.imax / 7) * 5))
		c.g -= (c.g % 200) / dt->crc * 2;
	if (dt->op.i > ((dt->op.imax / 9) * 5) &&
	dt->op.i < ((dt->op.imax / 7) * 6))
		c.b -= (c.b % 200) / dt->crc * 2;
	if (dt->op.i > ((dt->op.imax / 9) * 6) &&
	dt->op.i < dt->op.imax)
		c.r += (c.r % 200) / 7;
	dt->cbg = ncolor(c.r * 1.5, c.g * 1.5, c.b * 1.5);
	return (c);
}

void	unzoom(t_data *dt)
{
	dt->zoom *= 0.9;
}

t_color get_color(t_data *dt)
{
	t_color c;

	c = (dt->op.i == dt->op.imax? ncolor(0, 0, 0) :
	(icolor(dt, ncolor(dt->mc.r * dt->op.i / dt->op.imax,
	dt->mc.g * dt->op.i / dt->op.imax,
	dt->mc.b * dt->op.i / dt->op.imax))));
	c.r = (c.r < 10) ? c.r + 3 : c.r;
	c.g = (c.g < 10) ? c.r + 3 : c.g;
	c.b = (c.b < 10) ? c.r + 3 : c.b;
	c.r = (c.r > 240) ? c.r - 13 : c.r;
	c.g = (c.g > 240) ? c.r - 13 : c.g;
	c.b = (c.b > 240) ? c.r - 13 : c.b;
	return (c);
}

void	fill_bg(t_data *dt)
{
	int a;
	int b;

	a = 0;
	while (a <= dt->height)
	{
		b = 0;
		while (b <= dt->width)
		{
			put_ipxl(dt, dt->cbg, a, b);
			b++;
		}
		a++;
	}
}