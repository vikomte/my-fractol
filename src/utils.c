/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vic <vic@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 20:49:56 by vic               #+#    #+#             */
/*   Updated: 2016/02/25 23:35:45 by vic              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <fractol.h>

void	error(char e)
{
	if (e == 'u')
	{
		ft_putendl("usage: ./fractol 			[ T Y P E ]");
		ft_putendl(" ");
		ft_putendl("M A N D E L B R O T 	---> [ma...] || [M...]");
		ft_putendl("J U L I A               ---> [ju...] || [J...]");
		ft_putendl("B U R N I N G S H I P 	---> [bu...] || [B...]");
	}
	if (e == 'm')
	{
		ft_putendl("./fractol: error: mlx: le manque de fiabilité de la mlxlib a crée cette erreur...");
	}
	exit(1);
}

char valid(t_data *dt)
{
	if (((ssn(dt->arg, "ma") || ssn(dt->arg, "M")) && (dt->type = 'M')) ||
	((ssn(dt->arg, "ju") || ssn(dt->arg, "J")) && (dt->type = 'J')) ||
	((ssn(dt->arg, "bu") || ssn(dt->arg, "B")) && (dt->type = 'B')))
		return (1);
	return (0);
}

int  ssn(char *s, char *f)
{
	int a;
	int ta;
	int b;

	a = -1;
	while (s[++a] && !(b = 0))
	{
		ta = a;
		while (s[a++] == f[b++])
			if (!f[b])
				return (1);
		a = ta;
	}
	return (0);
}

void construct_f(t_data *dt)
{
	fill_bg(dt);
	dt->op.x = -1;
	while (++dt->op.x < dt->width)
	{
		dt->op.y = -1;
		while (++dt->op.y < dt->height)
		{
			(dt->type == 'J') ? julia(dt) : 0;
			(dt->type == 'M') ? mandelbrot(dt) : 0;
			(dt->type == 'B') ? burningship(dt) : 0;
		}
	}
}