[Fractal](https://en.wikipedia.org/wiki/Fractal) generator

Types of fractals:
* [Julia](https://en.wikipedia.org/wiki/Julia_set)
* [Mandelbrot](https://en.wikipedia.org/wiki/Mandelbrot_set)
* [Burning ship](https://en.wikipedia.org/wiki/Burning_Ship_fractal)

Features:
* MOUSE: zoom
* ARROWS: movement
* SPACE: freeze state

![fractol in action](f1.png)
![fractol in action](f4.png)
![fractol in action](f3.png)
![fractol in action](f2.png)